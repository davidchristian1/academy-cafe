import Foundation

// TODO: You may add/edit/remove any default code here

var inputAction: String = ""
var arrayOfMenu: [Menu] = [Menu(menuID: "F01", menuName: "Nasi Padang"), Menu(menuID: "F02", menuName: "Chicken Satay"), Menu(menuID: "F03", menuName: "Gado-Gado"), Menu(menuID: "B01", menuName: "Ice Tea"), Menu(menuID: "B02", menuName: "Mineral Water")]
var inputMenu: String = ""
var arrayOfShoppingCart: [ShoppingCart] = []

func showMainMenu() {
    print("""

    =================================
       Academy's Cafe & Resto v2.0
    =================================

    Options:
    [1] Buy Food
    [2] Shopping Cart
    [x] Exit

    """)
    
    print("Your choice? ", terminator: "")
}

func showShoppingCart(){
    print("Shopping Cart (\(arrayOfShoppingCart.count) items):")
    for item in arrayOfShoppingCart {
        print("\(item.foodQuantity) \(item.menuName)")
    }
}

while inputAction.lowercased() != "x"{
    showMainMenu()
    inputAction = readLine() ?? ""
    
    if inputAction == "1" {
        print("""
        Hi, we have 5 Food and Beverage option for you!
        -----------------------------------------------
        [\(arrayOfMenu[0].menuID)] \(arrayOfMenu[0].menuName)
        [\(arrayOfMenu[1].menuID)] \(arrayOfMenu[1].menuName)
        [\(arrayOfMenu[2].menuID)] \(arrayOfMenu[2].menuName)
        [\(arrayOfMenu[3].menuID)] \(arrayOfMenu[3].menuName)
        [\(arrayOfMenu[4].menuID)] \(arrayOfMenu[4].menuName)
        [Q] Back to Main Menu
        """)
        
        while inputMenu.lowercased() != "q" {
            print()
            print("Your F&B choice? ", terminator: "")
            
            inputMenu = readLine() ?? ""
            
            if inputMenu.lowercased() != "q" {
                var menuNameSame: String = ""
                for menu in arrayOfMenu{
                    if menu.menuID == inputMenu.uppercased(){
                        menuNameSame = menu.menuName
                        break
                    }
                }
                
                let shoppedItem = ShoppingCart(menuID: inputMenu.uppercased(), menuName: menuNameSame)
                
                print("How many \(menuNameSame) you want to buy? ", terminator: "")
                
                let amountOfFood = readLine() ?? "1"
                
                print()
                
                shoppedItem.foodQuantity = Int(amountOfFood) ?? 1
                
                arrayOfShoppingCart.append(shoppedItem)
                
                showShoppingCart()
            }
        }
        
    }
    else if inputAction == "2" {
        if arrayOfShoppingCart.count == 0 {
            print("Your shopping list is empty, please buy something")
        }
        else{
            showShoppingCart()
        }
    }
}

